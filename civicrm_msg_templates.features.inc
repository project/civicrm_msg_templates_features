<?php


/**
 * Implements hook_features_export_options().
 */
function civicrm_msg_templates_features_export_options() {
  $templates = civicrm_msg_templates_features_get_templates();

  return !empty($templates)
      ? $templates
      : array();
}

function civicrm_msg_templates_features_get_templates() {
  civicrm_initialize();
  $msgTpls = array();

  $messageTemplates = new CRM_Core_DAO_MessageTemplate();
  $messageTemplates->is_active = 1;
  $messageTemplates->is_default = 1;
  $messageTemplates->find();

  while ($messageTemplates->fetch()) {
    $key = $messageTemplates->msg_title;
    $msgTpls[$key] = $messageTemplates->msg_title . " (ID: " . $messageTemplates->id . ")";
  }
  asort($msgTpls);
  return $msgTpls;
}

/**
 * @param array $data
 *   this is the machine name for the component in question
 * @param array &$export
 *   array of all components to be exported
 * @param string $module_name
 *   The name of the feature module to be generated.
 * @return array
 *   The pipe array of further processors that should be called
 */
function civicrm_msg_templates_features_export($data, &$export, $module_name) {
  $export['dependencies']['features'] = 'features';
  $export['dependencies']['civicrm_msg_templates_features'] = 'civicrm_msg_templates_features';

  civicrm_initialize();
  $templates = civicrm_msg_templates_features_get_templates();

  foreach ($templates as $name => $label) {
    if (in_array($name, $data)) {
      // Make the font provider required
      $export['features']['civicrm_msg_templates'][$name] = $name;
    }
  }

  return $export;
}

/**
 * Render one or more component objects to code.
 *
 * @param string $module_name
 *   The name of the feature module to be exported.
 * @param array $data
 *   An array of machine name identifiers for the objects to be rendered.
 * @param array $export
 *   The full export array of the current feature being exported. This is only
 *   passed when hook_features_export_render() is invoked for an actual feature
 *   update or recreate, not during state checks or other operations.
 * @return array
 *   An associative array of rendered PHP code where the key is the name of the
 *   hook that should wrap the PHP code. The hook should not include the name
 *   of the module, e.g. the key for `hook_example` should simply be `example`.
 */
function civicrm_msg_templates_features_export_render($module_name, $data, $export = NULL) {
  $templates = civicrm_msg_templates_features_get_templates();

  $code = array();
  foreach ($data as $machine_name) {
    foreach ($templates as $name => $label) {
      if ($name == $machine_name) {
        $template = new CRM_Core_BAO_MessageTemplate();
        $template->msg_title = $name;
        $template->is_default = 1;
        $template->find(true);

        $obj = civicrm_msg_templates_features_simplify_object($template);
        if(isset($template->workflow_id)) {
          // Remove the FK id to workflow_id.
          unset($obj->workflow_id);

          // Get the workflow info that is a FK to civicrm_option_value and civicrm_option_group.
          $obj->_workflow_name = CRM_Core_DAO::getFieldValue('CRM_Core_DAO_OptionValue', $template->workflow_id);
          $workflow_group_id = CRM_Core_DAO::getFieldValue('CRM_Core_DAO_OptionValue', $template->workflow_id, 'option_group_id');
          $workflow_group_name = CRM_Core_DAO::getFieldValue('CRM_Core_DAO_OptionGroup', $workflow_group_id);
          $obj->_workflow_group_name = $workflow_group_name;
        }

        $code[$machine_name] = $obj;
      }
    }
  }
  $code = "  return " . features_var_export($code, '  ') . ";";
  return array('civicrm_msg_templates_default_templates' => $code);
}

function civicrm_msg_templates_features_simplify_object($civi_object) {
  $arr_version = (array)$civi_object;
  $keys = array_keys($arr_version);
  foreach($keys as $key) {
    if(stripos($key, '_') === 0 || in_array($key, array('N', 'id'))) {
      unset($arr_version[$key]);
    }
  }

  return (object)$arr_version;
}

/**
 * Implements hook_features_revert().
 */
function civicrm_msg_templates_features_revert($module) {
  // Run the revert process
  civicrm_msg_templates_features_rebuild($module);

}

/**
 * Implements hook_features_rebuild().
 */
function civicrm_msg_templates_features_rebuild($module) {

  $saved_templates = module_invoke($module, 'civicrm_msg_templates_default_templates');

  foreach ($saved_templates as $key => $template) {
    //$saved = fontyourface_save_font($font, TRUE); // Here it gets saved in the database -> TRUE overrides existing fonts.
    civicrm_msg_templates_features_save_template($template);
  }
}

